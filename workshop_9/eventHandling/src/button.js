const magicButton = document.getElementById('magic-button');
const magicButton2 = document.getElementById('magic-button2');
magicButton2.style.display = 'none';


const handleButtonHover = (event) => {
    const element = event.target;
    const red = Math.random() * 255;
    const green = Math.random() * 255;
    const blue = Math.random() * 255;

    element.style.background = `linear-gradient(rgb(${red}, ${green}, ${blue}), 
                                                rgb(${blue}, ${red}, ${green}))`;
    // console.log(event.target);
    // console.log('Mouse hover');
};



// document.body.addEventListener('mouseover', handleButtonHover);
magicButton.addEventListener('click', (event) => {
    if (magicButton2.style.display === 'none') {
        magicButton2.style.display = 'inline-block';
        magicButton.addEventListener('mouseover', handleButtonHover);
        magicButton2.addEventListener('mouseover', handleButtonHover);
        magicButton.setAttribute('data-clicks', 0);
    } else {
        const clicks = parseInt(magicButton.getAttribute('data-clicks'));

        if (clicks >= 5) {
            magicButton.style.display = 'none';
            magicButton2.style.display = 'none';

            const warning = document.createTextNode("You are trying to distrub the magic of the button!");
            // setTimeout(() => {
            //     alert('You are trying to distrub the magic of the button!')
            // }, 10);

            document.body.appendChild(warning);
        }
        magicButton.setAttribute('data-clicks', clicks + 1);
    }
})
magicButton2.addEventListener('click', (event) => {

})

