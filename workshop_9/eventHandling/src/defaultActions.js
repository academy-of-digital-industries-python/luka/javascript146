const a = document.getElementsByTagName('a')[0];
const form = document.getElementsByTagName('form')[0];
const formSubmitButton = form.getElementsByTagName('button')[0];
const div = document.getElementById('form-wrapper');
const mouseDiv = document.getElementById('mouse');
// on hover show website preview
console.log(a);

a.addEventListener('click', (event) => {
    event.preventDefault();
    console.log('I tricked you, You wont go to another site');
})


// form.addEventListener('submit', (event) => {
//     event.preventDefault();
//     event.stopPropagation();
//     console.log(event.target);
// })

formSubmitButton.addEventListener('click', (event) => {
    event.preventDefault();
    event.stopPropagation();

    console.log(event.target);
})

div.addEventListener('click', (event) => {
    // console.log(event.target);
    // if (event.target === div)
    alert('Hello from div');
})

// for (const child of div.childNodes) {
//     child.addEventListener('click', (event) => event.stopPropagation());
// }

window.addEventListener('mousemove', (event) => {
    // console.log(event.clientX, event.clientY);
    mouseDiv.style.left = `${event.clientX - 25}px`;
    mouseDiv.style.top = `${event.clientY - 25}px`;
})


window.addEventListener('keydown', (event) => {
    console.log(event.key)
})