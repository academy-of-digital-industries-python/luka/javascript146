"use strict";
/**
 * Data Structure: Array / მასივი
 * 
 * to save multiple digital data (მონაცემები)
 */

console.log(5); // 101
console.log(true); // 1
// Array of characters
console.log("Luka".toUpperCase()); // string - შემდგარი მნიშვნელობა

const age = 17;
// array declaration 
// Each value inside array is called Element
// enumerated (გადანომრილია) 0 -> N (element count) and it is called index
const friends = ["john", "gio", "hans", "gio", "Levan", "aleko"];
// what will be index of last element? 
// N - 1
const ages = [17, 28, 45, 33, 28, 29];

console.log(friends);
console.log(friends.length); // 5
// Accessing elements by index
console.log(friends[5]); // undefined | IndexOutOfRange

console.log('--- Make each name upper case ---')
for (let i = 0; i < friends.length; i++){
    // console.log(`${friends[i].toUpperCase()} ${ages[i]} years old`);
    let name = friends[i];
    name = `${name[0].toUpperCase()}${name.slice(1)}`;
    // re-assignment elements
    friends[i] = name;
    const age = ages[i];
    console.log(`${name} ${age} years old`);
}

// delete friends[friends.length - 1];
console.log(friends);
console.log('Pop (remove from the end)');
friends.pop();
console.log(friends);
console.log('Shift (remove from the start)');
friends.shift();
console.log(friends);

console.log('Push (insert to the end)');
friends.push('Givi');
console.log(friends);

console.log('Push (insert to the end)');
friends.push('Josh');
console.log(friends);

console.log('Unshift (insert to the end)');
friends.unshift('Tess');
console.log(friends);

ages.reverse();
console.log(ages);
ages.reverse();
console.log(ages);

console.log('Sort Ascending (ზრდადობით)');
ages.sort((a, b) => a - b);
console.log(ages);
console.log('Sort Descending (კლებადობით)');
ages.sort((a, b) => b - a);
console.log(ages);

ages[1] = 99;
console.log(ages);

console.log('indexOf');
console.log(friends.indexOf("Josh"));
console.log(friends.indexOf("Lela"));
console.log(friends.indexOf("Gio"));

console.log('Last index of')
console.log(friends.lastIndexOf("Gio"));

