function isEven(N) {
    // recursion
    N = Math.abs(N); // Absolute -> აბსოლიტური მნიშვნელობა |-7| = 7
    // if (N < 0) 
    //     return isEven(N * -1); // -97 -> 97
    if (N === 0) 
        return true;
    if (N === 1) 
        return false;
    
    return isEven(N - 2);
}
const N = -97; // --5 
console.log(isEven(N));