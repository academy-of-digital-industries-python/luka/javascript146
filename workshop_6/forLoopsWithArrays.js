const numbers = [-99, 44, 3, 234, 5, 63]; // mutable
for (const i in numbers) {
    console.log(`numbers[${i}] = ${numbers[i]}`);
}

for (const num of numbers) {
    console.log(num);
}


console.log('--- For each ---');
numbers.forEach(num => {
    console.log(num * 2);
});

numbers[4] = 7;

console.log('String mutability');
// mutability - რამდენად შეგვიძლია მნიშვნელობაში ცვლილების შეტანა
const firstName = "Gio"; // strings are immutable
firstName[0] = "L";

console.log(firstName); // Gio