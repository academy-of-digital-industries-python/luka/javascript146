/**
 * Higher order functions are type of functions that will receive function as a parameter
 * (Callback) and / or returns function 
 */

function generateRandomProduct(i) {
    return {
        title: `Product ${i}`,
        price: Math.floor((Math.random() * 1000) + 100)
    };
};

function generateRandomCar(i) {
    return {
        title: `Car ${i}`,
        price: Math.floor((Math.random() * 1000000) + 100000)
    };
};

function generateRandomArray(n, creator) {
    const arr = [];
    for (let i = 0; i < n; i++) {
        arr.push(creator(i));
    }
    return arr;
}

const products = generateRandomArray(10, generateRandomProduct);
const cars = generateRandomArray(20, generateRandomCar);


console.log(products[0]);
console.log(cars[0]);


// let productsTotalPrice = 0;

// products.forEach(product => {
//     productsTotalPrice += product.price;
// });

let productsTotalPrice = products
    .filter(({price}) => price < 500)
    .map(({price}) => price)
    .reduce((prevValue, currValue) => prevValue + currValue);

// let arr = products.filter(product => product.price < 500)
// console.log(arr);
// arr = arr.map(product => product.price);
// console.log(arr);
// arr = arr.reduce((prevValue, currValue) => prevValue + currValue);
// console.log(arr);


console.log(productsTotalPrice);



