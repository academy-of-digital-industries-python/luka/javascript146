/**
 * Data Structure Object
 * key-value pairs
 */

const student = {
    firstName: "Joe",  // property that holds data
    lastName: "Joe",
    age: 23,
    describeSelf: function() {
        // method / function
        console.log(`Hello I am ${this.firstName} ${this.lastName}`);
    }
};

// Accessing values
// console.log(student["firstName"]);
// console.log(student["lastName"]);
console.log(student.firstName);
console.log(student.lastName);
console.log(student.age);

console.log(student.father);

// Modifing values
student.firstName = "Jane";
console.log(student);

// Removing ky-vale pair
delete student.firstName; // removes property from the object
console.log(student);

// Adding new key-value pair
student.firstName = "Kyle";
student.pet = "Dog";
console.log(student);

// Object keys / ობიექტიდან მხოლოდ გასაღებების ამოღება
console.log('-- object keys --');
for (const key of Object.keys(student)) {
    console.log(key, student[key]);
}

console.log('-- object values --');
for (const value of Object.values(student)) {
    console.log(value);
}

console.log('-- object entries --');
for (const [key, value] of Object.entries(student)) {
    // const [key, value] = entry; // destructure
    // console.log(entry[0], entry[1]);
    console.log(key, value);
}

console.log(Object.entries(student));
student.describeSelf();