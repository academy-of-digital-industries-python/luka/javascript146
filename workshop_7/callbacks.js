/**
 * Callbacks are functions that will be called later by another function
 */

function doSomethingXTimes(callbackFn, times) {
    // Higher order function
    for (let i = 0; i < times; i++){
        callbackFn();
    }
}

function scream() {
    console.log('SCREAMING');
}

function greet() {
    console.log('Hello there');
}

doSomethingXTimes(scream, 10);
doSomethingXTimes(greet, 10);

// for (let i = 0; i < 10; i++){
//     scream();
// }
// for (let i = 0; i < 10; i++){
//     greet();
// }