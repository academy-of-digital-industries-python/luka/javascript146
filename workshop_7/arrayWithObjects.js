const cars = [
    {
        brand: 'Tesla',
        model: 'Model X',
        price: 35000
    },
    {
        brand: 'Tesla',
        model: 'Model Y',
        price: 65000
    },
    {
        brand: 'Mercedes',
        model: 'G5',
        price: 75000
    },
    {
        brand: 'BMW',
        model: 'X5',
        price: 80000
    }
];

let cheapestCar = cars[0];
for (const car of cars.slice(1)) {
    if (car.price < cheapestCar.price){
        cheapestCar = car; 
    }
}

console.log('Cheapest car in our store is', cheapestCar);

cheapestCar = cars.sort((car1, car2) => car1.price - car2.price)[0]; // 0
mostExpensiveCar = cars.sort((car1, car2) => car1.price - car2.price)[cars.length - 1];

console.log(cheapestCar, mostExpensiveCar);


