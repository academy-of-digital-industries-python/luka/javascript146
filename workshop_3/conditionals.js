const userName = "Valery";
const age = 16;


if (age >= 18) {
    // code block / fragment
    console.log(`${userName} is an Adult!`);
} else if (age == 16 && userName == "Valery") {
    console.log('cool');
} else {
    console.log(`${userName} is a Minor!`);
}

console.log('=== Weekdays ===');
const weekDay = 37;
if (weekDay == 1) {
    console.log('Sunday');
} else if (weekDay == 2) {
    console.log('Monday');
} else if (weekDay == 3) {
    console.log('Tuesday');
} else if (weekDay == 4) {
    console.log('Wednesday');
} else if (weekDay == 5) {
    console.log('Thursday');
} else if (weekDay == 5) {
    console.log('Friday');
} else if (weekDay == 6) {
    console.log('Saturday');
} else {
    console.log('Incorrect weekday!');
}

console.log('=== Random GPA ===');

const randomNumber = Math.floor(Math.random() * 101);
// const randomNumber = 100;
console.log(randomNumber);
if (randomNumber >= 90) {
    console.log('A');
} else if (randomNumber >= 80) {
    console.log('B');
} else if (randomNumber >= 70) {
    console.log('C');
} else if (randomNumber >= 60) {
    console.log('D');
} else if (randomNumber >= 50) {
    console.log('E');
} else {
    console.log('F');
}