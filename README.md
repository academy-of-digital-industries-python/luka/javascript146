# JavaScript146

- [Drive](https://drive.google.com/drive/u/0/folders/1xAM-3uJ489V_x_Tlo9sig8nDLgQIZV5C)

- [JavaScript Book](https://1drv.ms/b/s!AmZJMrBsKhiOh8UDJDRDATZCy9M9VA?e=ecsJjd)
## Homeworks
- [Homework 1](https://classroom.github.com/a/ZNgm42Tr)
- [Homework 2](https://classroom.github.com/a/akTfh6hr)
- [Homework 3](https://classroom.github.com/a/Kblx42C4)
- [Homework 4](https://classroom.github.com/a/AKz5eSy1)
- [Homework 4-1](https://classroom.github.com/a/Ty8F5NPV)
- [Homework 5](https://classroom.github.com/a/reFG4Xas)
- [Homework 6](https://classroom.github.com/a/tw22H8vQ)
- [Homework 7](https://classroom.github.com/a/2todRQ73)


- [Mini Projects](https://javascript-mini-projects.netlify.app/)
- [Final Projects](https://docs.google.com/document/d/18bhiu3Hw3u5fwKKegRrWufyKUs7zBhFJLYWYdG_77GE/edit?usp=sharing)