const signinForm = document.querySelector('form');
const [usernameField, passwordField] = signinForm.querySelectorAll('input');
const user = window.localStorage.getItem('user');
if (user) {
    window.location.href = './index.html';
}

const fieldError = (field, errorMessage) => {
    field.classList.add('is-invalid');
    field.nextElementSibling.innerText = errorMessage;
}

const usernameError = (errorMessage) => fieldError(usernameField, errorMessage);
const passwordError = (errorMessage) =>  fieldError(passwordField, errorMessage);

const validateForm = () => {
    const user = usernameField.value;
    const pass = passwordField.value;
    let formIsValid = true;

    if (user.length < 3) {
        usernameError('Username should be more than 3 letters!');
        formIsValid = false;
    } else {
        usernameField.classList.remove('is-invalid');
    }

    if (pass.length < 6) {
        passwordError('Password should be more than 8 letters!');
        formIsValid = false;
    } else {
        passwordField.classList.remove('is-invalid');
    }

    return [formIsValid, user, pass];
};

signinForm.onsubmit = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    const [formIsValid, user, pass] = validateForm();

    if (formIsValid) {
        console.log('send data to API', usernameField.value, passwordField.value);
        const response = await fetch('https://dummyjson.com/auth/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                username: user,
                password: pass
            })
        });

        const data = await response.json();
        if (data.message == 'Invalid credentials') {
            usernameError(data.message);
            passwordError(data.message);
        } else {
            usernameField.classList.add('is-valid');
            passwordField.classList.add('is-valid');
            window.localStorage.setItem('user', JSON.stringify(data));
            var now = new Date();
            var time = now.getTime();
            const expireTime = time + 1000 * 15;
            now.setTime(expireTime);
            document.cookie = `token=${data.token};expires=${now.toUTCString()}`;
            window.location.href = './index.html';
        }
        console.log(data);
    }
};
