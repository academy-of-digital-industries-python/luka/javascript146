const avatar = document.querySelector('img');
const usernamePlaceholder = document.querySelector('#username-placeholder');

const user = JSON.parse(window.localStorage.getItem('user'));
if (!user) {
    window.location.href = './signin.html';
}

avatar.src = user.image;
usernamePlaceholder.innerText = user.username;

document.querySelector('#signout-btn').onclick = () => {
    window.localStorage.removeItem('user');
    window.location.href = './signin.html';
};

function getCookieData(name) {
    let pairs = document.cookie.split("; "),
        count = pairs.length, parts;
    while (count--) {
        parts = pairs[count].split("=");
        if (parts[0] === name)
            return parts[1];
    }
    return false;
};

setInterval(() => {
    console.log(getCookieData('token'));
    if (!getCookieData('token')) {
        window.localStorage.removeItem('user');
        window.location.href = './signin.html';
    }
}, 5000)