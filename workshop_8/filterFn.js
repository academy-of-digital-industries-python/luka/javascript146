function myFilter(arr, filterCallbackFn) {
    const newArr = [];

    for (const item of arr) {
        if (filterCallbackFn(item)) {
            newArr.push(item);
        }
    }
    return newArr;
}

let nums = [1, 2, 3, 4, -9, -123];
let s = ["12312", "321453412", "dfgbfd", "hello", "hello"];

console.log(nums);
console.log(myFilter(nums, (num) => num > 0));
console.log(myFilter(s, (s) => s === 'hello'));