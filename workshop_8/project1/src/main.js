const body = document.body;
const h1 = body.children[0];
const lastP = body.children[2];

console.log(h1);
console.log(lastP);
console.log(lastP.nextSibling);
console.log(lastP.innerText);

console.log(Array.from(body.children));
console.log(Array.from(body.childNodes));
console.log(h1.parentNode);

// for (const child of body.children) {
//     console.log(child)
// }

Array.from(body.children).forEach(child => console.log(child));

console.log('=== Finding Elements ===');
console.log(document.getElementsByTagName('h1'));
console.log(document.getElementsByTagName('p'));
const someP = document.getElementById('someId');
console.log(someP);

setTimeout(() => {
    someP.innerText = "Hello from JavaScript";
}, 2000);

const infoPs = document.getElementsByClassName('info');


for (const p of infoPs) {
    // p.innerText = '<strong>Hello</strong>';
    p.onclick = (event) => {
        console.log(event.target, ' is being removed from HTML')
        event.target.remove();
    }
    p.innerHTML = '<strong>Hello</strong>';
}


// infoPs[infoPs.length - 1].remove();

const div = document.getElementById('p-wrapper');
const newP = document.createElement('p');
newP.innerText = "I will replace some p tag";
const newP1 = document.createElement('p');
newP1.innerText = "(SECOND) I will replace some p tag";

div.replaceChild(newP, div.children[2]);
// div.appendChild(newP1);
div.insertBefore(newP1, newP);

function createP(text) {
    let p = document.createElement("p");
    p.innerText = text;
    return p;
}

div.insertAdjacentElement('afterbegin', createP("AFTERBEGIN"));
div.insertAdjacentElement('beforebegin', createP("BEFOREBEGIN"));
div.insertAdjacentElement('afterend', createP("AFTEREND"));
div.insertAdjacentElement('beforeend', createP("BEFOREEND"));
