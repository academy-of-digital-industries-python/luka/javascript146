import { getSingleProduct } from "../api/endpoints.js";
import spinner from "../components/spinner.js";



const productPage = async (id) => {
    const loadingSpinner = spinner();
    document.body.appendChild(loadingSpinner);

    const product = await getSingleProduct(id);
    console.log(product);
    loadingSpinner.remove();

    const div = document.createElement('div');
    div.className = 'text-center mx-5';

    div.innerHTML = (`
        <h1>${product.title}</h1>
        <div class="row row-cols-2">
            <div id="product-images" class="row row-cols-2 col">
                <div class="flex flex-column col-2">

                </div>
                <div class="col-10">
                    <img src="${product.images[0]}" class="w-100" id="current-image"/>
                </div>
            </div>
            <div class="col text-start">
                <div class="badge bg-success">${product.price}</div>
                <p>${product.description}</p>
            </div>
        </div>
        
    `);

    const productImages = div.querySelector('#product-images');

    product.images.forEach((image, index) => {
        const imagesListDiv = productImages.querySelector('div');
        const img = document.createElement('img');
        img.width = "50";
        img.height = "50";
        img.style.objectFit = "cover";
        img.style.cursor = "pointer";

        img.className = 'rounded d-block border-primary';
        if (!index) {
            img.classList.add('border');
        }


        img.src = image;
        img.addEventListener('click', (event) => {
            const currentImage = productImages.querySelector('#current-image');
            for (const smallImage of imagesListDiv.querySelectorAll('img')) {
                smallImage.classList.remove('border');
            }

            currentImage.src = event.target.src;
            event.target.classList.add('border');

        })
        imagesListDiv.appendChild(img);
    });

    document.body.appendChild(div);
}
const urlParams = new URLSearchParams(window.location.search);

productPage(parseInt(urlParams.get('pid')));