"use strict";
import { productsGrid } from "../components/productsGrid.js";
import { fetchProducts, searchProduct } from "../api/endpoints.js";
import spinner from "../components/spinner.js";
// Render products to store page
let pGrid = null;

const doAllTheMagic = async () => {
    let products = [];
    try {
        const loadingSpinner = spinner();
        document.body.appendChild(loadingSpinner);

        const data = await fetchProducts();
        products = data.products;
        console.log(products);
        pGrid = productsGrid(products);
        pGrid.id = "products-grid";

        loadingSpinner.remove()
        document.body.appendChild(pGrid);
    } catch {
        window.location.href = './error.html';
    }
}

const searchForm = document.getElementById('product-search');
const searchInput = searchForm.getElementsByTagName('input')[0];

searchForm.addEventListener('submit', async (event) => {
    event.preventDefault();
    event.stopPropagation();

    pGrid.remove();
    const loadingSpinner = spinner();
    document.body.appendChild(loadingSpinner);

    const data = await searchProduct(searchInput.value);
    const products = data.products;
    pGrid = productsGrid(products);
    loadingSpinner.remove();
    document.body.appendChild(pGrid);
})

doAllTheMagic();
