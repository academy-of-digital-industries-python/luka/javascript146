import productCard from "./productCard.js";

const prodcutCards = (products) => {
    const grid = document.createElement('div');
    grid.className = 'row row-cols-2 row-cols-lg-5 g-2 g-lg-3';


    products.forEach(product => {
        const productCol = document.createElement('div');
        productCol.className = 'col';
        productCol.appendChild(productCard(product));
        
        grid.appendChild(productCol);
    });

    return grid;
};

const select = (options) => {
    const sortSelect = document.createElement('select');
    sortSelect.className = "form-select w-25";
    for (const {value, text} of options) {
        const optionTag = document.createElement('option');
        optionTag.innerText = text;
        optionTag.value = value;

        sortSelect.appendChild(optionTag);
    }
    return sortSelect;
}

export const productsGrid = (products) => {
    // prodcuts Grid
    const gridWrapper = document.createElement('div');
    gridWrapper.classList.add('p-3');

    const options = [
        {
            text: " - ",
            value: 0
        },
        {
            text: "Price ASC.",
            value: 1
        },
        {
            text: "Price DESC.",
            value: 2
        }
    ];
    const sortSelect = select(options);
    sortSelect.classList.add('mb-3');

    sortSelect.addEventListener('change', (event) => {
        let cards = document.getElementById('products-cards');

        gridWrapper.removeChild(cards);
        switch (event.target.value) {
            case "1":
                cards = prodcutCards(
                    products.sort((p1, p2) => p1.price - p2.price)
                );
                break;
            case "2":
                cards = prodcutCards(
                    products.sort((p1, p2) => p2.price - p1.price)
                );
                break;
            default:
                cards = prodcutCards(products.sort((p1, p2) => p1.id - p2.id));
                break;
        }
        cards.id = "products-cards"
        gridWrapper.appendChild(cards);
    })

    const cards = prodcutCards(products);
    cards.id = "products-cards"

    gridWrapper.appendChild(sortSelect);
    gridWrapper.appendChild(cards);
    return gridWrapper;
};