export const spinner = () => {
    const div = document.createElement('div');
    div.className = "text-center mt-5"

    div.innerHTML = (`
    <div class="spinner-border text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>
    `);

    return div;
};


export default spinner;
