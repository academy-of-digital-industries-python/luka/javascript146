
const carousel = (images) => {
    const div = document.createElement('div');

    div.innerHTML = (`
    <div id="carouselExampleIndicators" class="carousel slide w-25 mx-auto">
        <div class="carousel-inner" id="product-image-slider">
    
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon bg-primary rounded" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon bg-primary rounded" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    `);

    const productImageSlider = div.querySelector('#product-image-slider');

    images.forEach((imageURL, index) => {
        const slide = document.createElement('div');
        slide.className = 'carousel-item';
    
        if (index === 0)
            slide.classList.add('active');

        slide.innerHTML = (`<img src="${imageURL}" height="250" style="object-fit: cover;" class="d-block w-100">`);

        productImageSlider.appendChild(slide);
    })

    return div;
};

export default carousel;