export const productCard = (product) => {
    // definition
    // აგენერირებს ერთი კონკრეტული პროდუქტისთვის HTML ელემენტს!
    const productDiv = document.createElement('div');

    productDiv.className = 'card w-100 shadow-lg';
    productDiv.innerHTML = (`
        <img src="${product.thumbnail}" 
                class="card-img-top"
                width="200"
                height="200"
                style="object-fit: cover;"
                alt="${product.title}">
        <div class="card-body">
            <h5 class="card-title mb-5">${product.title}</h5>
            <span class="text-success">${product.price}$</span>
            <p class="card-text">
                ${product.description.slice(0, 60)}
            </p>
            <a href="./product.html?pid=${product.id}" class="btn btn-primary">Go somewhere</a>
        </div>
    `);
    return productDiv;
};


export default productCard;