import { API_BASE_URL } from "../config.js";
import { fetchData } from "./utils.js";

export const fetchProducts = async () =>  await fetchData(`${API_BASE_URL}/products/`);

export const searchProduct = async (productName) => await fetchData(`${API_BASE_URL}/products/search?q=${productName}`);

export const getSingleProduct = async (productId) => await fetchData(`${API_BASE_URL}/products/${productId}`);
