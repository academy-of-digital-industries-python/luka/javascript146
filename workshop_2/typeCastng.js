console.log(5 + Number("5")); // Type casting from string to number
console.log(5 + Number("5.5"));

console.log(5 + parseFloat("5.4"));
console.log(Math.round(parseFloat("5.5")));
console.log(typeof parseFloat("5.5"));
console.log(5 + parseInt("5.4"));

console.log("5" + String(5)); // 5 + "5"
console.log("Hello there " + String(true));

console.log('=== Casting to booleans ===')
console.log('\nNumbers')
console.log('1 in boolean: ', Boolean(1)); // true
console.log('0 in boolean: ', Boolean(0)); // false
console.log('-1 in boolean: ', Boolean(-1)); // true
console.log('-100 in boolean: ', Boolean(-100)); // true
console.log('100 in boolean: ', Boolean(100)); // true

console.log('\nStrings')
console.log('"" in boolean: ', Boolean("")); // false
console.log('"Hello" in boolean: ', Boolean("Hello")); // true
console.log('"1" in boolean: ', Boolean("1")); // true
console.log('"H" in boolean: ', Boolean("H")); // true
console.log('" " in boolean: ', Boolean(" ")); // true
