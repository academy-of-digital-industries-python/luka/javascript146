console.log(typeof 5); // number
console.log(typeof 5.1); // number
console.log(typeof -1); // number
console.log(typeof `-1`); // string
console.log(typeof "Hello"); // string
console.log(typeof 'Hello'); // string
