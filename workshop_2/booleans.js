// George Boole
console.log(true);
console.log(false);

// Comparison Operators
// equality / ტოლია?
console.log('=== Comparison ===')
console.log(`5 == 6 is ${5 == 6 + 1}`); // false
// inequality
console.log(`5 != 7 is ${5 != 7}`); // true
console.log(`5 < 7 is ${5 < 7}`); // true
console.log(`5 > 7 is ${5 > 7}`); // false
console.log(`5 <= 7 is ${5 <= 7}`); // true
console.log(`5 >= 7 is ${5 >= 7}`); // false
console.log(`7 >= 7 is ${7 >= 7}`); // true


// Logical Operators
console.log('=== Logial Operators ===');
console.log('\nAND Truth table (&&)');
console.log(`true and true is ${true && true}`); // true
console.log(`false and true is ${false && true}`); // false
console.log(`true and false is ${true && false}`); // false
console.log(`false and false is ${false && false}`); // false

console.log('\nOR Truth table (||)')
console.log(`true and true is ${true || true}`); // true
console.log(`false and true is ${false || true}`); // true
console.log(`true and false is ${true || false}`); // true
console.log(`false and false is ${false || false}`); // false

console.log('\nNOT Truth table (!)')
console.log(`not true is ${!true}`); // false
console.log(`not false is ${!false}`); // true