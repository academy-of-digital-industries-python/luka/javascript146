/**
 * for variable naming we use camelCase 
 * 
 * EXAMPLE:
 * firstName (camelCase)    ** use this
 * lastName                 ** use this
 * isRainingOutside
 * 
 * last_names (snake_case)
 * FirstName (PascalCase)
 * 
 * 
 * Don'ts: 
 * - don't use symbols (expection _)
 * - don't use numbers at the begining of the variable name
 * - use concise names 
 *      - no -> (a, b, c, x, y, p1, p2, u9)
 *      - yes -> firstName; lastName; userAge; healthPoint; products
 * 
 */
// declaration
let hello = ""
var firstName = "Josh"; 

console.log(firstName);
firstName = "John";
console.log(firstName);
firstName = "Lena";
console.log(firstName);
firstName = 17;
console.log(firstName);

// Introducting let && const
let lastName = "Doe";

lastName = "Dash";
console.log(lastName);
// constant - მუდმივი
const age = 37; // data type (მონაცემთა ტიპი) - number
// age = 9;
console.log(lastName);
console.log(typeof lastName);
console.log(typeof age);

