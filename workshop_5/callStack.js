function f4() {
    console.log('Executing f4');
    console.log('Done Executing f4');
}

function f3() {
    console.log('Executing f3');
    f4();
    console.log('Done Executing f3');
}

function f2() {
    console.log('Executing f2');
    f3();
    console.log('Done Executing f2');

}
function f1() {
    console.log('Executing f1');
    f2();
    console.log('Done Executing f1');
}


const mutatedF1 = () => {
    f4();
    f2();
    f3();
}

f1();
mutatedF1();