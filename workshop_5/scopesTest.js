
{
    const data = 5;
    {
        const data = 7;
        {  
            const data = 10;
            {
                console.log(data); // 10
            }
        }
        console.log(data); // 7
    }
    console.log(data); // 5
}
const data = 56;

console.log(data); // 56