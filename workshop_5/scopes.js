/**
 * Scopes (Local && Global)
 * ხილვადობის არეალი
 */

// Global Scope

function retrieveData() {
    // Local scope
    console.log('From Global Scope: ', data);

    // სერვერიდან მოაქვს მონაცემები
    return Math.floor((Math.random() * 100) + 50);
}

function calculate(data) {
    // Local scope
    // Shadowing / გადაფარვა
    let s = 0;
    for (let i = data; i < Math.pow(data, 2); i++) {
        s += i;
    }
    return s;
}

function analyzeData() {
    // Local scope
    const data = retrieveData();  // local variable
    console.log('Data from local scope ', data);
    const calculation = calculate(data);
    return calculation;
}

const data = 2;  // Global variable
console.log(calculate(data));
console.log(calculate(3));


const calculation = analyzeData();
console.log(calculation);