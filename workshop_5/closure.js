const f = (a, b) => a + b;

// const g = (a, b) => () => f(a * 2, b - 9);

function g(x, y) {
    return function() {
        // closure
        return f(x * 2, y - 9);
    }
}


console.log(f(7, 9)); // 7 + 9 = 16

const someF = g(7, 9);
// console.log(
//     g(7, 9)()
// ); // (a * 2) + (b - 9)

console.log(someF());
console.log(someF());
console.log(someF());
console.log(someF());
