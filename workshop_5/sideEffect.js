let a = 5;

function changeGlobal() {
    // this function has a side effect
    // impure
    // changes state
    a++; // a = a + 1; a += 1
}

function withoutSideEffect() {
    // Pure
    return a + 1;
}


changeGlobal();
console.log(a);
changeGlobal();
console.log(a);
changeGlobal();
console.log(a);
changeGlobal();
console.log(a);


a = withoutSideEffect();
console.log(a);
console.log(withoutSideEffect());
console.log(a);
