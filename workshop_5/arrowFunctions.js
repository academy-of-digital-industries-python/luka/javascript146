/**
 * Annonymus Functions
 */

// function greet(user) {
//     console.log(`Hello ${user}`);
// }

// const greet = (user) => {
//     console.log(`Hello ${user}`);
// };

const greet = function(user = "Anonnymus") {
    console.log(`Hello ${user}`);
};

greet("Luka");
greet();