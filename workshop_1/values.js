// Data Type: numbers

console.log(5); // 5
console.log(5 + 6); // 11
console.log(5 / 2); // 2.5
console.log(5 - 3); // 2
console.log(5 * 7); // 35
// modulo / ნაშთის ოპერატორი
console.log(5 % 2); // 1

console.log(Infinity);
console.log(-Infinity);

// Not a Number
console.log(NaN);


console.log(5 + 9 * 8 - 7); // 70
console.log((5 + 9) * 8 - 7); // 70

console.log(0.9 + 0.1); // 1
console.log(0.2 + 0.1); // 0.3
console.log((20 + 10) / 100) // 0.3