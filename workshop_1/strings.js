console.log("\"Hello\"\n'world!'"); // escape characters
console.log('\'Hello\'\\\\"world!"');
// backticks
console.log(`Hello world!`);

// console.log('My Diary:\nToday I went to work and had a meeting\nHad fun!');
console.log(`My Diary:
I went to work
Had fun`);

// concatenation 
console.log("Hello" + " " + "Joe");
// template literals
console.log(`I am ${21 + 1} years old!`);