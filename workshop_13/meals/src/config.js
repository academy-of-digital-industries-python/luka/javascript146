const API_BASE_URL = 'https://www.themealdb.com/api/json/v1/1';


export const config = {
    api: {
        searchMeal: `${API_BASE_URL}/search.php`,
        randomMeal: `${API_BASE_URL}/random.php`,
        categories: `${API_BASE_URL}/categories.php`,
    }
};

export default config;