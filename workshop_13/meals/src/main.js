import config from "./config.js";

const mealFinderForm = document.querySelector('#meal-finder-form');
const mealFinderInput = mealFinderForm.querySelector('#meal-finder-input');
const mealsDiv = document.querySelector('#meals');

// const fetchCategories = async () => {
//     const response = await fetch('https://www.themealdb.com/api/json/v1/1/categories.php');
// }
const searchMeal = async () => {
    const url = `${config.api.searchMeal}?s=${mealFinderInput.value}`;
    const response = await fetch(url);
    const { meals } = await response.json();

    mealsDiv.innerHTML = '';
    meals.forEach(meal => {
        const mealDiv = document.createElement('div');
        mealDiv.innerHTML = (`
        <img src="${meal.strMealThumb}" width=150/>
        `);
        mealsDiv.appendChild(mealDiv);
    });
};

mealFinderForm.onsubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();

    searchMeal();
};


