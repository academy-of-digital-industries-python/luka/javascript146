class Animal {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    get_details() {
        return `${this.name} is ${this.age} years old`;
    }
};


class LandAnimal extends Animal {
    constructor(name, age, fur) {
        super(name, age);
        this.fur = fur;
    }

    walk() {
        console.log(this.get_details(), 'is walking and has', this.fur, 'fur');
    }
}

const dog = new LandAnimal('doggo', 4, 'golden');
const cat = new Animal('catto', 3);
dog.walk();
console.log(dog.get_details());
console.log(cat.get_details());


