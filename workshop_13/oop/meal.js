class Meal {
    constructor(id, title, category, thumbnail) {
        // data / attributes
        this.id = id;
        this.title = title;
        this.category = category;
        this.thumbnail = thumbnail;
    }

    describe() {
        // functionality
        console.log(this.title, "yummy");
    }

    cook() {
        console.log(this.title, "cooking");
    }
};

class MealsAPI {
    // static attribute
    static API_BASE_URL = 'https://www.themealdb.com/api/json/v1/1'

    static async serachMeal(mealName) {
        // static method
        const response = await fetch(`${MealsAPI.API_BASE_URL}/search.php?s=${mealName}`);
        const data = await response.json();
        return data;
    }
};

const eggSoup = new Meal(35, "Egg Soup", "Asian", "some_link"); // object / instance of a Meal class

const meals = [
    new Meal(35, "Egg Soup", "Asian", "some_link"), // object
    new Meal(36, "Fried Eggs", "Asian", "some_link"), // object
    new Meal(37, "Beef Burger", "American", "some_link"), // object
]

meals.forEach(meal => meal.describe());

console.log(MealsAPI.API_BASE_URL);
const s = async () => {
    const data = await MealsAPI.serachMeal('soup');
    console.log(data.meals[0]);
}

s();