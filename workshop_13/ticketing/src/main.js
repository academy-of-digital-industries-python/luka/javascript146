const seat = document.querySelector('#seat-1');

const c = () => Math.random() * 255;

const reservePlace = (e) => {
    // as many times as it is clicked
    e.stopPropagation();
    const r = c();
    const g = c();
    const b = c();
    const color = `rgb(${r}, ${g}, ${b})`;
    e.target.style.background = color;
    localStorage.setItem('color', color);
};

seat.style.background = localStorage.getItem('color');

// if (localStorage.getItem('reserved') === '1'){
//     // one once
//     // when site is loaded
//     seat.style.background = '';
// }
seat.onclick = reservePlace;