const clock1 = document.getElementById('clock-1');
const clock2 = document.getElementById('clock-2');

setInterval(() => {
    const timeNow = new Date();
    clock1.innerText = timeNow.toLocaleString('ka-GE', {timeZone: 'Asia/Tbilisi'});
}, 1000);

setInterval(() => {
    const timeNow = new Date();
    clock2.innerText = timeNow.toLocaleString('en-GB', {timeZone: 'Europe/London'});
}, 1000);

