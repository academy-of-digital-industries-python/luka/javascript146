
console.log('Starting calculations...');

setTimeout(() => {
    let sum = 0;
    for (let i = 0; i < 1000; i++){
        sum += 1;
    }
    console.log(sum);
    console.log('Done calculations...');
    // 1 sec
}, 1000 + Math.random() * 1500);

console.log('Starting new calculations...');

setTimeout(() => {
    sum = 0;
    for (let i = 0; i < 1000; i++){
        sum -= 1;
    }
    console.log(sum);
    console.log('Done new calculations...');
    // 1 sec
}, 1000 + Math.random() * 1500);

