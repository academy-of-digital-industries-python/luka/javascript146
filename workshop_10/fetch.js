const APIUrl = 'https://dummyjson.com/products';
const productsDiv = document.getElementById('products-div');

const createProdcutDiv = (product) => {
    const div = document.createElement('div');
    div.classList.add('col');
    div.innerHTML = (`
        <div class="card w-100 shadow-lg">
            <img src="${product.thumbnail}" 
                    class="card-img-top"
                    width="200"
                    height="200"
                    style="object-fit: cover;"
                    alt="${product.title}">
            <div class="card-body">
                <h5 class="card-title mb-5">${product.title}</h5>
                <span class="txet-success">${product.price}$</span>
                <p class="card-text">
                    ${product.description.slice(0, 60)}
                </p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
    `)
    return div;
}
const fetchAndRenderProducts = async () => {
    try {
        let response = await fetch(APIUrl);

        const data = await response.json();
        console.log(data);
        // const firstProduct = data.products[0];

        // response = await fetch(`https://dummyjson.com/products/${firstProduct.id}`);
        // console.log(await response.json());
        for (const product of data.products) {
            productsDiv.appendChild(createProdcutDiv(product));
        }
    } catch {
        document.body.appendChild(document.createTextNode("Something went wrong :("));
    }
}

async function fetchUsers() {
    try {
        const response = await fetch('https://dummyjson.com/users');

        const data = await response.json();
        console.log(data);
        data.users.forEach(user => {
            document.body.appendChild(document.createTextNode(user.firstName));
        })
    } catch {
        document.body.appendChild(document.createTextNode("Something went wrong :("));
    }
}
// fetch(APIUrl)
//     .then((response) => response.json())
//     .then((data) => {
//         // render to HTML
//         for (const {title, thumbnail} of data.products) {
//             const div = document.createElement('div');
//             div.innerHTML = (`
//                 <p>${title}</p>
//                 <img src="${thumbnail}"/>
//             `)
//             document.body.appendChild(div);
//         }
//     })
//     .catch((error) => console.log('Something went wrong!'));
console.log('Before firing up asnyc ');

fetchAndRenderProducts();
console.log('I am syncronous');
fetchUsers();
console.log('I am syncronous');