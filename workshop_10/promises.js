const promise = new Promise((success, failure) => {
    // Producing code
    console.log('Started Cooking burger');
    setTimeout(() => {
        const rndNum = Math.random() * 100; // [0; 100]
        if (rndNum >= 50) {
            success(rndNum);
        }
        else {
            failure(rndNum);
        }
    }, Math.random() * 5000);
});

const promise2 = new Promise((success, failure) => {
    // Producing code
    console.log('Started Cooking gulao');
    setTimeout(() => {
        const rndNum = Math.random() * 100; // [0; 100]
        if (rndNum >= 70) {
            success(rndNum);
        }
        else {
            failure(rndNum);
        }
    },  Math.random() * 5000);
});


promise.then(
    // Consuming code
    (rndNum) => { // callback
        // Success
        console.log(rndNum);
        console.log('Eating my delicious food!');
    },
    (rndNum) => {
        // Failure
        console.log(rndNum);
        console.log('This is outrageous!');
    }
)

// promise2.then(
//     (rndNum) => {
//         // Success
//         console.log(rndNum);
//         console.log('Eating my delicious chinese food!');
//     },
//     (rndNum) => {
//         // Failure
//         console.log(rndNum);
//         console.log('This is outrageous (chinese)!');
//     }
// )
promise2
    .then((rndNum) => console.log('Eating my delicious chinese food!'))
    .catch((rndNum) => console.log('This is outrageous (chinese)!'))