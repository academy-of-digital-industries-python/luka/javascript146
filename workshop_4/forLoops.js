for (let i = 0; i < 30; i++) {
    console.log(`Start of iteration ${i + 1}`);
    console.log(`Checking ${i}`);

    // თუ (ჭეშმარიტი) { შეასრულებს ამ ბლოკს }
    if (i % 2 || i % 9) { // false || false => false; 1 || 1 => 1 => true
        // guard caluse
        console.log(`${i} not divisible by 9 or 2 or both`);
        console.log(`End of iteration ${i + 1} because next command is "continue"`);
        continue;
    }
    console.log(`${i} is divisible by 9 and 2`);
    /**
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    console.log(`End of iteration ${i + 1}`);

}
    


// let i = 0;
// while (i < 10) {
//     console.log(i);
//     i++;
// }


