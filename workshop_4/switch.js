let weekDay = 1; // 1 - 7

if (weekDay === 1)
    console.log('Monday');
else if (weekDay === 2)
    console.log('Tuesday');
else if (weekDay === 3)
    console.log('Wednesday');
else if (weekDay === 4)
    console.log('Friday');
else if (weekDay === 5)
    console.log('Thursday');
else if (weekDay === 6)
    console.log('Saturday');
else
    console.log('Sunday');


switch (weekDay) {
    case 1:
        console.log('Monday');
        break;
    case 2:
        console.log('Tuesday');
        break;
    case 3:
        console.log('Wednesday');
        break;
    case 4:
        console.log('Thursday');
        break;
    case 5:
        console.log('Friday');
        break;
    case 6:
        console.log('Saturday');
        break;
    default:
        console.log('Sunday');
        break;
}