// definition of the function 
// ფუნქციის განსაზღვრა
function greet() { 
    // function body / block
    // ფუნქციის ტანი / ბლოკი
    console.log('გამარჯობა, როგორ ხარ მეგობარო?');
}

function cleanRoom() {
    console.log('power Robot on');
    console.log('Robot is scanning the room');
    console.log('Robot started cleaning');
    for (let i = 0; i < 10; i++) {
        console.log('Robot is cleaning');
    }
    console.log('Robot finished cleaning');
    console.log('power Robot off');
}

greet(); // functin invoke / call / ფუნქციის გამოძახება!
// greet();
// greet();
// greet();
// console.log(greet);

for (let i = 0; i < 3; i++){
    cleanRoom();
}
// cleanRoom();
// cleanRoom();
// cleanRoom();

