console.log((false || true) && (true || false)); // true

// truthy value?
// values that are true
// (-inf, 0) U (0, +inf)
// "hello"
// " "
// falsy value?
// ""
// values that are false
console.log(0 || 7 || 8 || 0); // 7

console.log(5 && 0 && 9 && 0); // 0
console.log(0 || -43);
console.log(1 && 0);
