function addition(a, b) {
    // console.log(typeof a, typeof b);
    // console.log(a, b); // undefined if no arguments are passed
    // console.log((a + b) * 2);
    return (a + b) * 2; // returns value where it was called
}

function max(number1, number2) {
    // if (number1 > number2)
    //     return number1;
    
    // return number2;
    // ternary operator
    // პირობა ? (თუ ჭეშმარიტია რა დააბრუნოს) : (თუ მცდარია მაშინ რა დააბრუნოს)
    return number1 > number2 ? number1 : number2; 
}

const result = addition(5, 6); // positional arguments
console.log(addition(1, 2));
addition(3, 2);
addition(7, 8);
console.log(max(11, 9));